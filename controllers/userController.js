const express = require('express');
const mongoose = require('mongoose');
const User = mongoose.model('User')

var router = express.Router();

router.get('/',(req, res)=>{
    res.render("user/addEdits" , {
        viewTitle :" Ajouter un manager "
    });
})

router.post('/', (req,res)=>{
    insertRecord(req ,res );
  // console.log(req.body)
});

function insertRecord(req, res){
    var user = new User();
    user.email = req.body.email
    user.firstname = req.body.firstName
    user.lastname = req.body.lastName
    user.mobile = req.body.telephone;

    user.save((err ,doc)=>{
        if(!err)
            res.redirect('user/list');
        else{

                console.log('Error during record insertion ')
        }
    })

}

router.get('/list' , (req,res)=>{
   

    User.find((err,docs)=>{
      
        console.log(docs)
        if(!err){
            res.render("user/list",{
                list: docs
                
            });
        }

        else{
            console.log("Error in retrieving user list")
        }
        
    });
});

router.post('/login',(req ,res)=>{
    res.redirect("user/list");

    
}) 

router.get('/home', (req, res)=>{
    res.render('pages/home')
})

router.get('/login' , (req , res )=>{
    res.render('pages/login' , )
})

module.exports = router;