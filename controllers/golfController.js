const express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const Golf = mongoose.model('Golf');

router.get('/', (req,res)=>{
    res.render("golf/addOrEdit", {
        viewTitle : "Insert Golf"
    });
});

router.post('/', (req,res)=>{
    if(req.body._id == '')
        insertRecord(req,res);
        else
        updateRecord(req, res);
});

function insertRecord(req,res) {
    var golf = new Golf();
    golf.titre = req.body.titre;
    golf.latitude = req.body.latitude;
    golf.longitude = req.body.longitude;
    golf.description = req.body.description;
    golf.manager = req.body.manager;
    golf.save((err,doc)=> {
        if(!err) 
            res.redirect('golf/list');
        else {
            console.log('Error during record insertion : '+ err);
        }
    });
}

function updateRecord(req,res) {
    Golf.findOneAndUpdate({_id: req.body._id}, req.body, {new: true}, (err, doc)=> {
        if(!err) { res.redirect('golf/list');}
        else {
            console.log('Error during record update : '+ err);
        }
    });
}

router.get('/list', (req,res)=>{
    // res.json('from list');
    Golf.find((err,docs)=>{
        if(!err) {
            res.render("golf/list", {
                list: docs
            });
        }
        else {
            console.log('Error in retrieving golf list : '+err);
        }

        console.log(docs);
    });


});

router.get('/:id', (req,res)=>{ 
    Golf.findById(req.param.id, (err, doc)=> {
        if(!err) {
            res.render("golf/addOrEdit", {
                viewTitle : "Update Golf",
                golf: doc
            });
        } 
    });
});

router.get('/delete/:id',(req,res) => {
    Golf.findByIdAndRemove(req.params.id, (err, doc) => {
        if(!err) {
            res.redirect('/golf/list');
        }
        else { console.log('Error in golf delete :'+ err);}
    });
});

module.exports=router ;