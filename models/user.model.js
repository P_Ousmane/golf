const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
        
    },  
   
    firstname: {
        type: String
    },
    lastname: {
        type: String
    },
    admin: {
        type: Boolean
    },
    mobile : {
        type:Number
    },
}, {
    timestamps: true
});

module.exports = mongoose.model('User', userSchema);