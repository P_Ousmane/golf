const mongoose = require('mongoose');

var golfSchema = new mongoose.Schema({
    titre: {
        type: String,
    },
    latitude: {
        type: Number
    },
    longitude: {
        type: Number
    },
    description: {
        type: String
    },
    manager: {
        type: String
    }
});


mongoose.model('Golf', golfSchema);